import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  private _parent: any;


  @Input() set parent(value: any) {
    this._parent = value;
  }

  get parent(): any {
    return this._parent;
  }

  nameParrent :string = this._parent.name
  
  
  

  

}
